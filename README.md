# GraphQL server in Rust with Warp, Juniper and different DBs

This repo is the reference for my blog posts on hwo to build a GraphQL server in Rust with the Warp web framework,
Juniper and various DB engines.

## Posts and repo tags

You can inspect the code used for each article by switching to the corresponding tag in the git repo.

- [Rust GraphQL webserver with Warp, Juniper and MongoDB](http://alex.amiran.it/post/2018-08-16-rust-graphql-webserver-with-warp-juniper-and-mongodb.html) (tag: v-001)


