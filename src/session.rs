use redis::{self, Commands};
use serde_json::{to_string as to_json, from_str as from_json};
use model::User;

pub struct Session {
	redis: redis::Client,
}

impl Session {
	pub fn new(url: &str) -> Session {
		Session {
			redis: redis::Client::open(url).unwrap()
		}
	}

	pub fn get(&self, id: &str) -> Option<User> {
		let conn = self.redis.get_connection().unwrap();
		let user: Option<String> = conn.get(make_key(id)).unwrap();
		user.map(|u| from_json::<User>(&u).unwrap())
	}

	pub fn set(&self, id: &str, user: &User) -> bool {
		let conn = self.redis.get_connection().unwrap();
		let user: String = to_json(user).unwrap();
		let _: () = conn.set(make_key(id), &user).unwrap();
		true
	}

	pub fn del(&self, id: &str) -> bool {
		let conn = self.redis.get_connection().unwrap();
		let _: () = conn.del(make_key(id)).unwrap();
		true
	}
}

fn make_key(session: &str) -> String {
	let mut key = "session:".to_owned();
	key.push_str(session);
	key
}
